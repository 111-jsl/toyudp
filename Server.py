from NewReno import NewReno
from threading import Thread
import os
from defines import client_addr, printf, del_file, data_dir, debug, inner_server_addr
import defines

class Server(Thread):
    def __init__(self, inner_server_addr):
        super().__init__(target=self.ConnHandle, daemon=True)
        self.protocol = NewReno(inner_server_addr, 'server', 0)
        self.inner_server_addr = inner_server_addr
        self.ports = [i for i in range(18000, 27000, 1)]
    
    def __del__(self):
        self.protocol.socket.close()
    
    def port_alloc(self):
        return self.ports.pop()
    
    def close(self, conn):
        conn.socket.close()
        self.ports.append(conn.host_addr[1])
    
    def ConnHandle(self):
        while True:
            print("create new handle thread and bind it to port...")
            conn = NewReno((self.protocol.host_addr[0], self.port_alloc()), 'server', 0)
            conn, conn_type = self.protocol.accept(conn)
            assert(conn_type != 0)
            t = Thread(target=self.RequestHandle, args=(conn, conn_type,))
            t.start()
            
            
            
    def RequestHandle(self, conn, conn_type):
        if conn_type == 'download':
            file = conn.echo_request()
            conn.fsend(file)
            self.close(conn)
            
        elif conn_type == 'upload':
            file = data_dir + conn.frecv()
            self.close(conn)
            if del_file:
                print("md5sum %s" % file)
                print(os.popen('md5sum %s' % file).read())
                os.remove(file)
        exit(0)
        
    
        
        
            
        


if __name__ == "__main__":
    s = Server(inner_server_addr)
    s.start()
    while True:
        cmd = input()
        if cmd == 'quit':
            break
        elif cmd == 'debug':
            defines.debug ^= True
        elif cmd == 'del_file':
            defines.del_file ^= True
        else:
            print(os.popen(cmd).read())
    print("done")
        
        
        
    
    