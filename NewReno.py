import socket
import os

import FDFTPsocket
from FDFTP import Fdftp
# from fixedint import , , 
from defines import printf, data_dir, encode_style, wtime_tot
from time import sleep




class NewReno(Fdftp):
    def __init__(self, host_addr, type, target_addr):
        super().__init__(host_addr, type)
        self.cwnd = 2 # 先传文件名
        self.ssthresh = 1000
        self.sendbase = (0)
        self.dupcnt = 0
        self.buffer = dict()
        self.retrans = []
        self.max_seq = 0
        self.target_addr = target_addr


    def meta_init(self):
        self.cwnd = 2
        self.ssthresh = 1000
        self.sendbase = (0)
        self.dupcnt = 0
        self.buffer = dict()
        self.retrans = []
        self.max_seq = 0
        
        
    def request(self, file_name):
        self.init_header()
        data = file_name.encode(encode_style)
        # data = file_name
        wait_time = 0
        self.header['data_len'] = (len(data))
        self.header['flgs'] = self.mask['req']
        while wait_time < wtime_tot:
            self.socket.sendto(self.pack_fix(data), self.target_addr)
            try:
                data, addr = self.socket.recvfrom(self.headbufsize)
                head = self.unpack_head(data)
                if head['flgs'] & self.mask['reqack']:
                    print('request success')
                    return 0
                else:
                    wait_time += 1
                    print("file not found in server")
            except socket.timeout as e:
                wait_time += 1
                print(e)
        
        print('server not respond, request failed, abort')
        return -1
        
        

    
    
    
    
    def echo_request(self):
        self.init_header()
        print("listening request")
        while True:
            try:
                data,client_addr = self.socket.recvfrom(self.bufsize)
                break
            except socket.timeout as e:
                print(e)
                continue
        print("get request")
        head, data = self.unpack_fix(data)
        if client_addr != self.target_addr or not (head['flgs'] & self.mask['req']) or head['flgs'] & self.mask['syn']:
            print("illegal request")
            print("new addr: %s != target addr: %s" % (client_addr, self.target_addr))
            return -1
        # 接收请求，看看有没有这个文件
        
        file = data[:head['data_len']].decode(encode_style, 'ignore')
        # file = data[:head['data_len']]
        print(file)
        for r, d, f in os.walk(data_dir):
            # print(r)
            if r == data_dir:
                for fn in f:
                    # print(fn)
                    if fn == file:
                        # print(self.target_addr)
                        self.send_head(self.mask['reqack'], self.target_addr)
                        print("file found in %s" % file)
                        # sleep(10)
                        return file
       
        self.send_head(self.mask['clear'], self.target_addr)
        print("file not found, abort")
        return -1
       
    
    
    
    
    
    
    def fsend(self, file_name):
        self.init_header()
        self.meta_init()
        
        
        print("opening file...")
        file_len = os.path.getsize(data_dir + file_name)
        print("file_len: %d" % file_len)
        seg_len = 0
        if file_len % self.mss:
            seg_len = file_len - (file_len % self.mss) + self.mss
        else:
            seg_len = file_len
        f = open(data_dir + file_name, 'rb')
    
        
        # 传输文件名
        print("sending filename...")
        assert(len(file_name) > 0 and len(file_name) < self.mss)
        
        data = (file_name + ' ' + (str)(file_len)).encode(encode_style)
        # data = (file_name + ' ' + (str)(file_len))
        # self.header['request_content'] = len(data)
        
        
        task = FDFTPsocket.Task(data_dir + file_name)
        wait_time = 0
        while wait_time < wtime_tot:
            self.send_fix(task, data, self.mask['start'], self.target_addr)
            try:
                data,server_addr = self.socket.recvfrom(self.headbufsize)
                break
            except socket.timeout as e: # 没有收到ack，超时重传
                self.header['seq_num'] -= self.mss
                print(self.header)
                wait_time += 1
                print("filename timeout")
                print("resending...")
        if wait_time >= wtime_tot:
            print("server not respond, abort")
            return -1
        
        print("sending file contents...")
        after_retrans = False
        end = False
        frflg = False
        rto_time = 0
        self.header['seq_num'] = self.init_seq
        state = 'EG'
        
        
        while True:
            last_state = state
            # 窗口大于阈值，设置congestion avoiding
            if self.cwnd >= self.ssthresh:
                state = 'CA'
            else:
                state = 'EG'
            
            
            # sending
            end_offset = 0
            i = 0
            while i < self.cwnd:
                # print("i: %d\t cwnd: %d" % (i, self.cwnd))
                if len(self.retrans) > 0: # 重传retrans列表
                    # self.retrans.sort()
                    after_retrans = True
                    f.seek(self.retrans[0], 0)
                    self.header['seq_num'] = self.retrans[0]
                    del self.retrans[0]
                    data = f.read(self.mss)
                elif after_retrans: # 重传完，文件指针回到一开始的地方
                    after_retrans = False
                    if end:
                        end_offset = self.cwnd - i
                        break
                    f.seek(self.max_seq + self.mss, 0)
                    self.header['seq_num'] = self.max_seq + self.mss
                    continue
                else: # 正常传输
                    data = f.read(self.mss)
                    # print("ftell %d" %f.tell())
                    # print(file_len)
                    if f.tell() >= file_len:
                        printf("end coming")
                        self.header['flgs'] = self.mask['end']
                        end_offset = self.cwnd - i - 1
                        i = self.cwnd
                        end = True
                        
                # print(data)
                printf("%d send..." % self.header['seq_num'])
                self.send_fix(task, data, self.header['flgs'], self.target_addr)
                i += 1

            
            # receiving
            i = end_offset
            while i < self.cwnd:
                printf("sendbase:%d"%self.sendbase)
                # print(self.buffer)
                # print(self.retrans)
                try:
                    data,server_addr = self.socket.recvfrom(self.headbufsize)
                except socket.timeout as e:
                    printf("timeout waiting ack")
                    state = 'RTO'
                    break
                rto_time = 0
                head = self.unpack_head(data)
                self.max_seq = max(self.max_seq, head['seq_num'])
                printf("%d recv..." % head['ack_num'])
                # 超出FR窗口，是上一次send但没recv的包
                # if (self.max_seq < head['seq_num']) and frflg:
                #     continue
                # 重复的ack
                if head['ack_num'] == self.sendbase:
                    self.buffer[head['seq_num']] = True# sender seq num
                    
                    self.dupcnt += 1
                    if self.dupcnt >= 3:
                        self.dupcnt = 0
                        printf("dupack 3!")
                        state = 'FR'
                        break
                # 正常ack
                elif head['ack_num'] == self.sendbase + self.mss:
                    self.sendbase += self.mss
                    while self.sendbase in self.buffer:
                        self.buffer.pop(self.sendbase)
                        self.sendbase += self.mss
                # 回来ack大于sendbase，跟上服务器的节奏
                elif head['ack_num'] > self.sendbase:
                    self.sendbase = head['ack_num']
                    tmp = dict()
                    for k, v in self.buffer.items():
                        if k >= self.sendbase:
                            tmp[k] = v
                    self.buffer = tmp
                            
                i += 1
                
            if self.sendbase >= seg_len:
                break
            # frflg = False
            # state transition
            if last_state == 'FR':
                self.cwnd -= 3
            if state == 'EG':
                self.cwnd *= 2
                # frflg = False
            elif state == 'CA':
                self.cwnd += 1
            elif state == 'RTO':
                rto_time += 1
                if rto_time > wtime_tot:
                    print("long time no respond, abort")
                    break
                self.ssthresh = max(self.cwnd // 2, 2)
                self.cwnd = 1
                self.retrans.append(self.sendbase)
            elif state == 'FR':
                # frflg = True
                # self.cwnd = 1
                self.cwnd //= 2
                self.ssthresh = max(self.cwnd, 2)
                self.cwnd += 3
                # 重传
                self.retrans.clear()
                iter = self.sendbase
                maxbuf = max(self.buffer.keys())
                while iter < maxbuf:
                    if iter not in self.buffer.keys():
                        self.retrans.append(iter)
                    iter += self.mss
                

        print("send done")
        f.close()
        # assert(len(self.buffer) == 0)
        print("buffer size: %d" % len(self.buffer))
        print("retrans size: %d" % len(self.retrans))
        # assert(len(self.retrans) == 0)
        task.finish()
        return 0
                
                        
    def frecv(self):
        self.init_header()
        self.meta_init()
        print("listening request...")
        while True:
            try:
                data,client_addr = self.socket.recvfrom(self.bufsize)
                break
            except socket.timeout as e:
                print(e)
                continue
        
        print("get request")
        head, data = self.unpack_fix(data)
        
        if client_addr != self.target_addr or not (head['flgs'] & self.mask['start']) or head['flgs'] & self.mask['syn']:
            print("illegal request")
            print("new addr: %s != target addr: %s" % (client_addr, self.target_addr))
            return -1
        self.header['dst_port'] = (client_addr[1])
        self.header['seq_num'] = self.init_seq
        file_stuff = data.decode(encode_style, 'ignore')[:head['data_len']]
        # file_stuff = data[:head['data_len']]
        file_name, file_len = file_stuff.split(' ')
        file_len = (int)(file_len)
        print(len(file_name))
        
        f = open(data_dir + file_name, 'wb')
        self.socket.sendto(self.pack_head(), client_addr)
        self.header['seq_num'] = self.init_seq
        
        end = False
        
        while not end or len(self.buffer):
            printf("buffer size: %d" % len(self.buffer))
            printf("sendbase: %d" % self.sendbase)
            # get data
            while True:
                try:
                    data,client_addr = self.socket.recvfrom(self.bufsize)
                    break
                except socket.timeout as e:
                    print(e)
                    continue
            # unpack data
            head, data = self.unpack_fix(data)
            printf("%d recv..." % head['seq_num'])
            # 如果之前的名字包，接收方发的ack丢失了，那么现在这个包是发送方重发的名字包
            # if head['flgs'] & self.mask['start']:
            #     continue
            # handle data 1
            if head['seq_num'] == self.sendbase:
                self.sendbase += self.mss
                self.header['ack_num'] = self.sendbase
                printf(head['data_len'])
                f.write(data[:head['data_len']])
                while self.sendbase in self.buffer:
                    f.write(self.buffer[self.sendbase])
                    self.buffer.pop(self.sendbase)
                    self.sendbase += self.mss
                
                    
            elif head['seq_num'] > self.sendbase:
                self.header['ack_num'] = self.sendbase
                printf("dupcnt")
                self.buffer[head['seq_num']] = data[:head['data_len']]
            else:
                printf("garbage")
                self.header['ack_num'] = head['seq_num'] + self.mss
                printf("%d send..." % self.header['ack_num'])
                self.socket.sendto(self.pack_head(), client_addr)
                continue
            # handle data 2
            # if head['seq_num'] == 9216:
            #     print("flgs:%d"%head['flgs'])
            
            if head['flgs'] & self.mask['end']:
                self.header['flgs'] = self.mask['end']
                end = True
            # handle data 3
            self.header['seq_num'] = head['seq_num']
            # send back data
            printf("%d send..." % self.header['ack_num'])
            self.socket.sendto(self.pack_head(), client_addr) 
            if self.sendbase > file_len + self.mss:
                break    
            
        print("receive done")
        f.close()
        return file_name
        
                        
                        