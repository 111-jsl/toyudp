# mode: debug / release
debug = True
# 接收端接收文件、FileGen生成文件只会打印md5sum，打印完会把文件删掉
del_file = False

# only for str output
def printf(str):
    if debug:
        print(str)

# addr
# 客户端没有绑定端口，系统会后续自动分配，然后再发给服务器。这里20没有用处
client_addr = ('172.30.206.216', 20) 
server_addr = ('8.210.173.76', 7728)
inner_server_addr = ('172.19.6.181', 7728)
# 传输的各种数据所在的mulu
data_dir = './data/'

# 专门用来测试download test的
test_file = 'thread_file_download_test.py'

encode_style = 'utf-8'
# 多少次timeout退出
wtime_tot = 6