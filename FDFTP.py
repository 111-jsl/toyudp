import socket
import struct
# from fixedint import , , 




class Fdftp(object):
    def __init__(self, host_addr, type):
        # header meta data
        self.mss = (1400)
        self.init_seq = (0)
        self.header_len = 20
        self.host_addr = host_addr
        self.headbufsize = self.header_len
        self.bufsize = self.mss + self.header_len + 24
        
        # server data
       
        
        # format
        # self.int_max = 4.2e9
        self.packet_struct = struct.Struct('20s1400s')
        self.head_struct = struct.Struct('HHIIHBBHH')
        self.header_format = \
        (
            "src_port", "dst_port",
            "seq_num",
            "ack_num",
            "data_len", "flgs", "rwnd", #flgs: "unused_bits", "u", "a", "p", "r", "s", "f"
            "checksum", "request_content"
        )
        
        
        self.header_val = \
        (
            (host_addr[1]), (0),
            (self.init_seq),
            (self.init_seq),
            (0), (0), (200),
            (0), (0)
        )
    
            
        self.mask = \
        {
            "quit": (0x40),
            
            "start": (0x20),
            "end": (0x10),
            
            "ack": (0x8),
            "syn": (0x4),
            
            "req": (0x2),
            "reqack": (0x1),
            "clear": (0x0)
        }
        
        # set up real data and socket
        self.header = dict(zip(self.header_format, self.header_val))
        socket.setdefaulttimeout(6)
        self.socket = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        recv_buff = self.socket.getsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF)
        send_buff = self.socket.getsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 1000000)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, 1000000)
        print("recv_buff: %d --> %d" % (recv_buff, 1000000))
        print("send_buff: %d --> %d" % (send_buff, 1000000))
        
        
        if type == 'server':
            self.socket.bind(host_addr)
            
    def init_header(self):
        # self.header['src_port'] = self.host_addr[1]
        # self.header['dst_port'] = 0
        self.header['seq_num'] = 0
        self.header['ack_num'] = 0
        self.header['data_len'] = 0
        self.header['flgs'] = 0
        self.header['rwnd'] = 0
        self.header['checksum'] = 0
        self.header['request_content'] = 0

    def check_outrange(self):
        print(self.header.values())
        assert(self.header['src_port'] < 65000)
        assert(self.header['dst_port'] < 65000)
        assert(self.header['seq_num'] < (int)(4.2e9))
        assert(self.header['ack_num'] < (int)(4.2e9))
        assert(self.header['data_len'] < 65000)
        assert(self.header['flgs'] < 256)
        assert(self.header['rwnd'] < 256)
        assert(self.header['checksum'] < 65536)
        assert(self.header['request_content'] < 65536)
        
        
        
    
    def unpack_fix(self, data):
        unpacked_data = self.packet_struct.unpack(data)
        head = self.head_struct.unpack(unpacked_data[0])
        data = unpacked_data[1]
        head = dict(zip(self.header_format, head))
        return head, data
    
    def pack_fix(self, data):
        # self.check_outrange()
        head = self.head_struct.pack(*self.header.values())
        return self.packet_struct.pack(*(head, data))

    def unpack_head(self, data):
        unpacked_data = self.head_struct.unpack(data)
        head = dict(zip(self.header_format, unpacked_data))
        return head
    
    def pack_head(self):
        # self.check_outrange()
        return self.head_struct.pack(*self.header.values())
        
    def send_head(self, flgs, addr):
        self.header['data_len'] = (0)
        self.header['flgs'] = flgs
        self.socket.sendto(self.pack_head(), addr)
        self.header['seq_num'] += 1
    
    def send_fix(self, task, data, flgs, addr):
        self.header['data_len'] = (len(data))
        # print('datalen: %d'%self.header['data_len'])
        self.header['flgs'] = flgs
        task.sendto(self.socket, self.pack_fix(data), addr)
        self.header['seq_num'] += self.mss
          
        
    def close(self):
        self.socket.close()
        self.ports
    
    # conn_type = {req, clear}
    def connect(self, server_addr, conn_type):
        self.init_header()
        self.header['src_port'] = self.host_addr[1]
        self.header['dst_port'] = server_addr[1]
        while True:
            # first time SYN SENT
            self.send_head(self.mask['syn'] | self.mask[conn_type], server_addr)
            self.header['src_port'] = self.socket.getsockname()[1]
            # self.socket.bind((self.host_addr[0], self.header['src_port']))
            # print(self.socket.getsockname())
            print("assigned: %d" % self.header['src_port'])
            data = 0
            _server_addr = 0
            try:
                data,_server_addr = self.socket.recvfrom(self.headbufsize)
            except socket.timeout as e:
                print("timeout error")
                print("reconnecting...")
                continue
            
            head = self.unpack_head(data)
            
            if (_server_addr == server_addr) and (head['flgs'] & self.mask['syn']) and (head['flgs'] & self.mask['ack']) and (head['ack_num'] == self.header['seq_num']):
                # second time ESTAB
                self.header['ack_num'] = head['seq_num'] + 1
                self.send_head(self.mask['ack'], server_addr)
                self.header['dst_port'] = head['request_content']
                
                print("connection set up")
                return (server_addr[0], head['request_content'])
            else:
                continue
            # print("request invalid")
            # return -1
            

            
    def accept(self, conn):
        self.init_header()
        conn_type = 0
        client_addr = self.host_addr
        while True:
            print("listening to port...")
            while True:
                try:
                    data, client_addr = self.socket.recvfrom(self.headbufsize)
                except socket.timeout as e:
                    # print(e)
                    continue
                print("1st recv")
                # assert(client_addr != self.host_addr)
                conn.target_addr = client_addr
                head = self.unpack_head(data)
                if head['flgs'] & self.mask['syn']:
                    #first time
                    if head['flgs'] & self.mask['req']:
                        conn_type = 'download'
                    elif head['flgs'] & self.mask['start']:
                        conn_type = 'upload'
                    else:
                        print("conn_type unfound, disconnected")
                        continue
                    self.header['dst_port'] = client_addr[1]
                    self.header['ack_num'] = head['seq_num'] + 1
                    self.header['request_content'] = (conn.host_addr[1])
                    self.send_head(self.mask['syn'] | self.mask['ack'], client_addr)
                    break
            
            
            wait_time = 0
            
            while wait_time < 3:
                try:
                    data,_client_addr = self.socket.recvfrom(self.headbufsize)
                except socket.timeout as e:
                    print(e)
                    wait_time += 1
                    continue
                print("2nd recv")
                print((client_addr, _client_addr, head['src_port']))
                head = self.unpack_head(data)
                
                if (client_addr != _client_addr):
                    print("not consistant")
                    wait_time += 1
                    continue
                if head['flgs'] & self.mask['ack']:
                    break
            
            if wait_time >= 3:
                continue
            else:
                break
            
        # key = (self.host_addr, client_addr)
        # self.session_table[key] = True
        print("connection set up")
        return conn, conn_type
    
    
    
        
        
    def fsend(self, file_name):
        NotImplementedError(self.fsend)
                
                
    def frecv(self):
        NotImplementedError(self.frecv)
                
                
                
                
                
