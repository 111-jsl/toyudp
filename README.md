# 传输层实验：FTFTP

## 大概的结构图：

![](E:\fudan\junior\Computer_Network\outline.drawio.svg)

## 框架介绍：

1. 服务端设置一个固定的端口，充当前台的作用，专门用来负责处理客户端的连接请求，并创建新的线程和端口，提供给客户端
2. 这里设计是一次连接只做一个操作：客户端发来的连接会带有flgs，除了一般的tcp握手flgs以外，还会带其他的flgs用来表明这个连接是用来干嘛的。
   1. flg = req，那就是向服务端表明自己想要下载文件；
   2. flg = start，那就是表明自己想要上传文件
3. 服务端前台收到flgs后会根据上面的编码查看客户端想做什么，并设置conn_type = "download" or "upload"，然后创建线程和端口，把这个conn_type作为参数传进去，告诉线程它要做什么。
4. 关于端口的设置：服务端是固定的20，客户端第一次send后系统自动分配，第二次把真正端口发给服务端

## 自己debug时用的测试介绍：

位于Client.py里，有threadx_filex_download_test和threadx_filex_upload_test，作用是创建x个线程去下载或上传x个文件，**线程数量必须小于等于文件数量**，否则会报assert错误，传参就是(线程数，文件数)

测试里的文件是FileGen自动生成的，用户事先指定文件大小，这里测试里的上传文件大小统一都是41MB，下载文件是已经在服务端的./data目录下预先准备好了，注意**所有上传下载的数据都需要放在data里**，并且**不能文件夹嵌套**

## 使用手册：

注意：如果客户端本地有重名文件，无法从服务端下载；服务端有的话同样也无法接收客户端的上传

所以**使用前先把客户端的data文件夹清空，服务端保留**（用来测试用）

客户端启动./Client.py，进入shell，可以输入各种命令：

1. download:下载文件，需要提供文件名，这个文件需要在服务端存在
2. upload:上传文件，需要提供文件名
3. upload_test:执行threadx_filex_upload_test，需要传入参数：线程数 文件数
4. download_test:执行threadx_filex_download_test，需要传入参数：线程数 文件数
5. debug:反转debug位
   1. debug = True: 输出调试内容
   2. debug = False: 不输出
6. del_file:反转del_file位
   1. del_file = True: 
      1. 一般测试，threadx.....test，客户端下载文件，服务端接收文件，都不保留文件，直接输出md5sum值，
   2. del_file = False:
      1. 如果为false，就不删除也不输出md5sum
7. quit:
   1. 退出shell
8. 其他指令，可以执行任何能在shell上跑的指令，比如ls，它会输出当前文件夹的文件



服务端启动./Server.py，进入shell，可以输入各种命令：

1. debug:反转debug位
   1. debug = True: 输出调试内容
   2. debug = False: 不输出
2. del_file:反转del_file位
   1. del_file = True: 
      1. 一般测试，threadx.....test，客户端下载文件，服务端接收文件，都不保留文件，直接输出md5sum值，
   2. del_file = False:
      1. 如果为false，就不删除也不输出md5sum
3. quit:
   1. 退出shell
4. 其他指令，可以执行任何能在shell上跑的指令，比如ls，它会输出当前文件夹的文件



## 问题

1. 如果开始传了，基本上能保证传对，到最后可能有一方没收到ack，这里设计是4次time out就abort
2. 多次传输，可能前一次的接收缓冲区没有清空，下一次的连接会出现很多次的无效连接，需要耐心的等一下，看到connect set up就连接成功了
3. goodput和score的差距比较大，说明做了很多重复的发包，性能上不太好
4. 只在本地的虚拟机对传，换一个环境可能会出现各种各样的问题，由于时间原因未能解决，可以多重试几次
5. 由于时间原因，文档可能没有完整的介绍实验代码，可以继续查看源代码



