import random
from defines import data_dir


class FileGen(object):
    def __init__(self, size) -> None:
        self.size = size
    def __call__(self, n):
        flist = []
        print("generating...")
        for i in range(n):
            file_name = 'fg%d.py' % i
            flist.append(file_name)
            print("processing %s" % file_name)
            self.gen(file_name)
        print("done")
        return flist

    def gen(self, file_name):
        f = open(data_dir + file_name, 'w')
        for j in range(0, self.size, 1024):
            c = random.choice('abcdefghijklmnopqrstuvwxyz!@#$%^&*()')
            f.write(c*(min(j + 1024, self.size) - j))
        f.close()
        return 0