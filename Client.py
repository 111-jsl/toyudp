import sys
import os
from NewReno import NewReno
from FileGen import FileGen
from threading import Thread
from defines import client_addr, server_addr, del_file, test_file, data_dir
import defines




class Client(Thread):
    def __init__(self, client_addr, runtype):
        super().__init__()
        self.runtype = runtype
        self.protocol = NewReno(client_addr, 'client', 0)
        self.client_addr = client_addr
    
    def __del__(self):
        self.protocol.socket.close()
        
    def setparam(self, server_addr, flist):
        self.server_addr = server_addr # 服务器前台
        self.flist = flist
        # self.n = n

    def upload(self, file):
        ret = -1 #防止服务器还在等第二次握手，无法响应客户端
        while ret:
            t_addr = self.protocol.connect(self.server_addr, 'start')
            print(t_addr)
            assert(t_addr != -1)
            self.protocol.target_addr = t_addr
            ret = self.protocol.fsend(file)
        
    def download(self, file_name):
        # ret = -1
        t_addr = self.protocol.connect(self.server_addr, 'req')
        print(t_addr)
        assert(t_addr != -1)
        self.protocol.target_addr = t_addr
        ret = self.protocol.request(file_name)
        if ret == -1:
            print("download failed")
            return -1
        file = data_dir + self.protocol.frecv()
        if del_file:
            print("md5sum %s" % file)
            print(os.popen('md5sum %s' % file).read())
            os.remove(file)
        return 0
        

    def run(self):
        if self.runtype == 'download':
            for file in self.flist:
                self.download(file)
        elif self.runtype == 'upload':
            for file in self.flist:
                self.upload(file)
        # exit(0)
    
    
        

def threadx_filex_upload_test(t, n):
    print("thread%d_file%d_upload_test start" % (t, n))
    assert(t <= n)
    file_size = (int)(4.1e7)
    file_num = n
    fg = FileGen(file_size)
    flist = fg(file_num)
    
    
    batch = n // t
    tpool = []
    for i in range(t):    
        c = Client(client_addr, 'upload')
        c.setparam(server_addr, flist[i*batch: (i+1)*batch])
        tpool.append(c)
        c.start()
    for thread in tpool:
        thread.join()
    if del_file:
        for file in flist:
            file = data_dir + file
            print("md5sum %s" % file)
            print(os.popen('md5sum %s' % file).read())
            os.remove(file)
    
    print("thread%d_file%d_upload_test pass" % (t, n))
    
def threadx_filex_download_test(t, n):
    print("thread%d_file%d_download_test start" % (t, n))
    assert(t <= n)
    flist = []
    for i in range(n):
        flist.append(test_file)
    print(flist)
    batch = n // t
    tpool = []
    for i in range(t):
        c = Client(client_addr, 'download')
        c.setparam(server_addr, flist[i*batch: (i+1)*batch])
        tpool.append(c)
        c.start()
    for thread in tpool:
        thread.join()
    
    print("thread%d_file%d_download_test pass" % (t, n))
            




if __name__ == "__main__":
    # threadx_filex_test(1, 1)
    # threadx_filex_test(1, 3)
    # threadx_filex_test(3, 3)
    c = Client(client_addr, 'unrun')
    c.setparam(server_addr, 0)
    while True:
        cmd = input()
        if cmd == 'download':
            file_name = input('file_name: ')
            c.download(file_name)
        elif cmd == 'upload':
            file_name = input('file_name: ')
            c.upload(file_name)
        elif cmd == 'upload_test':
            t, n = map(int, input('thread_num : file_num : ').split())
            threadx_filex_upload_test(t, n)
        elif cmd == 'download_test':
            t, n = map(int, input('thread_num : file_num : ').split())
            threadx_filex_download_test(t, n)
        elif cmd == 'debug':
            defines.debug ^= True
        elif cmd == 'del_file':
            defines.del_file ^= True
        elif cmd == 'quit':
            break
        else:
            print(os.popen(cmd).read())
    print("done")
    
    
    
        

    


    

